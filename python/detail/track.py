__author__ = 'bpytko'

from math import pi
from bisect import bisect_right


class Piece(object):
    def __init__(self, end=0.0, curv=0.0):
        self.end = end
        self.curv = curv

    def __lt__(self, other):
        return self.end < other.end

    def __gt__(self, other):
        return self.end > other.end

    def __repr__(self):
        return "({0}, {1})".format(self.end, self.curv)


class Track(object):
    def __init__(self, track):
        self.pieces = [Piece()]
        for piece in track['pieces']:
            end = self.pieces[-1].end
            self.pieces.append(self.create_piece(end, piece))

    # change this to facilitate lanes
    def curvature(self, x):
        x %= self.pieces[-1].end
        return self.pieces[bisect_right(self.pieces, Piece(x))].curv

    # change this to facilitate lanes
    def abs_position(self, piece, x_piece):
        return self.pieces[piece].end + x_piece

    @staticmethod
    def create_piece(end, piece):
        if "length" in piece:
            return Piece(end + piece['length'], 0)
        else:
            return Piece(end + piece['radius'] * pi * piece['angle'] / 180,
                         1.0 / piece['radius'])

    # perhaps json dumps
    def __repr__(self):
        return '[' + ','.join([piece.__repr__() for piece in self.pieces]) + ']'