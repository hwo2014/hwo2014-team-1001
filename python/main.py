import json
import socket
import sys
from detail.track import *


class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.track = Track({'pieces': []})
        self.key = key
        self.color = ""
        self.pos = 0
        self.v = 0
        self.curv_int = 0


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def create_race(self, car_count=1):
        self.msg("createRace",
                 {
                     "botId": {
                         "name": self.name,
                         "key": self.key
                     },
                     "trackName": "keimola",
                     "password": "dupa",
                     "carCount": car_count
                 })

    def join_race(self, custom_name, car_count):
        self.msg("joinRace",
                 {
                     "botId": {
                         "name": custom_name,
                         "key": self.key
                     },
                     "trackName": "keimola",
                     "password": "dupa",
                     "carCount": car_count
                 })
        self.msg_loop()

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_car_init(self, data):
        print("Car color is: " + data['color'])
        self.color = data['color']

    def on_game_init(self, data):
        self.track = Track(data['race']['track'])
        print(self.track )
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        params = [x for x in data if x['id']['color'] == self.color][0]
        i = params['piecePosition']['pieceIndex']
        x = params['piecePosition']['inPieceDistance']
        pos = self.track.abs_position(i, x)
        self.v = pos - self.pos
        self.pos = pos
        curv_ahead = abs(self.track.curvature(pos + 50))
        self.curv_int = (self.curv_int + curv_ahead) / 3
        th = 1 / (1 + 200 * self.curv_int)
        print(self.curv_int)
        print(th)
        if th < 0.3:
            th = 0.3
        self.throttle(th)


    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_car_init,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
            print(line)


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key, race = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        if race == "true":
            print("Creating race")
            bot.create_race(1)
        bot.join_race(bot.name, 1)




        #class Race(object):
        #    def __init__(self, host="testserver.helloworldopen.com", port=8091, players=1, password=""):
        #        self.host = host
        #        self.port = port
        #        self.players = players
        #        self.password = password
        #
        #    def create(self, car_count=1):
        #        self.msg("createRace",
        #                 {
        #                     "botId": {
        #                         "name": self.name,
        #                         "key": self.key
        #                     },
        #                     "trackName": "keimola",
        #                     "password": "dupa",
        #                     "carCount": car_count
        #                 })
        #
        #class Communication(object):
        #    def __init__(self, socket):
        #        self.socket = socket
        #
        #    def msg(self, msg_type, data):
        #        self.send(json.dumps({"msgType": msg_type, "data": data}))
        #
        #    def send(self, msg):
        #        self.socket.send(msg + "\n")